<?php

class Utility {

	public static function clean_input(string $input): string {
		$input = trim($input);
		$input = htmlspecialchars($input);
		$input = stripslashes($input);
		return $input;
	}
}