<?php

class Connection {
	public static function connect_to_database(): PDO {
		$db = "tp_mvc_201";
		$dsn = "mysql:host=localhost;port=3306;dbname=$db;";
		$user = "root";
		$pass = "";
		
		try {
			$pdo = new PDO($dsn, $user, $pass);
			// echo "connected to $db" . PHP_EOL;
			return $pdo;
		} catch (PDOException $err) {
			die("Something went wrong " . $err->getMessage() . PHP_EOL);
		}
	}
}


